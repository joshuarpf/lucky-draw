<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', function() {
  Auth::logout();
  return redirect('/');
})->name('logout');

// Admin
Route::post('/admin/draw', 'AdminController@draw');
Route::get('/admin/token', 'AdminController@showToken');

// User
Route::post('/user/add', 'UserController@add');
Route::get('/user/numbers', 'UserController@numbers');