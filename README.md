 # Lucky Draw

 This is a sample laravel application intended to demonstrate my knowledge in the framework. I have been using this framework on and off, for various monolithic applications. It is not designed in a client-server application, but a simple web app with a laravel backend and reactjs on the front. 

**To whom it may concern**
As I was informed that the primary purpose of this test is to prove my knowledge in the framework and backend development, I took a different approach in building the application. Normally, I would have 2 separate applications runnig in containers, where one is a the backend, and the other is the front-end client.

I would normally use Laravel as a backend framework, providing nothing but API endpoints. I do not utilize the "view" portions of it. I will have controllers that function as resource, and are mostly invokable, and my routes will be implementing RESTful API. As for the front end, it will be a separate application running on nodejs and react. Authentication will be implementing JWT (Jason Web Tokens) JWT, where the session is mostly on the front end. 

This time, as mentioned above, I created it in such a way that I simply replaced the built in vue, with react ( and typescript ), and played it like one single web application. Authentication is mostly handled by Laravel itself. 

Having said all that, I know that there is a lot of things that can be improved on on the front end, but I am sure that the backend does prove my knowledge in the framework. 

My style in building applications with laravel is to break pieces down into modules ( Libraries folder in this one ), and have the controllers do as little as possible. 

# Requirements
1. Server with web server (apache or nginx) , configured  for this app
2. PHP 7.4 
3. Composer to be installed to that server
4. Mysql database server
5. Database named "lucky_draw" 

# How to run
1. Clone the repo
2. rename the "env-working-example" file into .env
3. Replace mine with your configuration 
4. Run "composer install "
5. Run "npm install"
6. Run "npm run dev"
7. Run "php artisan migrate" - to populate the database
8. Run "php artisan db:seed" - to create sample data
9. Test the app by loging in with "admin@luckydraw.com" and "password" as password. 

**All the created accounts use 'password' as password** 