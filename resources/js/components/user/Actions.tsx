import axios from 'axios'
import React from 'react'

export const AddNumber = (number: string) =>  {
  return axios.post(
    process.env.MIX_APP_URL + '/user/add', {
      number
    }
  ).then((result) => {

    if(result && result.status === 200 && result.data) {
      return { status: result.status, data: result.data }
    }
  }).catch((error: any) => {
    if(error.response.status === 422) {
      return { status: error.response.status, data: error.response.data.errors }
    }
  })
}

export const GetNumbers = () =>  {
  return axios.get(
    process.env.MIX_APP_URL + '/user/numbers'
  ).then((result) => {
    if(result && result.status === 200 && result.data) {
      return result.data
    }
  })
}