import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import TextField from "@material-ui/core/TextField"
import Typography from '@material-ui/core/Typography'
import React, { useEffect, useState } from 'react'
import { isValidEntry } from "../Common"
import { AddNumber, GetNumbers } from './Actions'
import { useStyles } from './Styles'

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

// tslint:disable-next-line:no-empty
function UserDashboard() {
  const classes = useStyles()

  const [numberInput, setNumberInput] = useState("")
  const [numberInputErrorMessage, setNumberInputErrorMessage] = useState("")
  const [numberInputError, setNumberInputError] = useState(false)
  const [dataDisplay, setDatadisplay] = useState([])

  const callAdd = async () => {
    const addResult = await AddNumber(numberInput)

    if( addResult ) {
      if( addResult.status === 200 ) {
        refreshList()
      } else if ( addResult.status === 422 ) {
        if ( addResult.data.number && addResult.data.number[0] ) {
          setNumberInputErrorMessage(addResult.data.number[0])
        }
      }
    }
  }

  const updateNumberInput = (value: string): void => {
    setNumberInput(value)
    if ( isValidEntry(value) ) {
      setNumberInputError(false)
    } else {
      setNumberInputError(true)
    }
  }

  const refreshList = () => {
    GetNumbers().then((result) => {
      setDatadisplay(result)
    }).catch((error) => {
      console.log(`Error in fetching numbers: `, error)
    })
  }

  useEffect(() => {
    // Update the document title using the browser API
    refreshList()
  }, dataDisplay)

  return(
    <Card className={ classes.root }>
      <CardContent>
        <Typography gutterBottom variant="h4" component="h2">
          Your Numbers
        </Typography>
        <hr />
      </CardContent>
      <CardContent>
        <TextField
          helperText={ (numberInputError) ? 'Invalid entry': '' }
          error={ numberInputError }
          id="outlined-basic"
          label="Enter a number"
          variant="outlined"
          value={ numberInput }
          onChange={ (event) => updateNumberInput(event?.target.value) }
        />
        <div>
          <span style={{ color: 'red', fontWeight: 'bolder', marginTop: '10px' }}>{ numberInputErrorMessage }</span>
        </div>
      </CardContent>
      <CardActions>
        <Button disabled={ numberInputError } color="primary" variant="contained" onClick={() => { callAdd() }}>Save</Button>
      </CardActions>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Number</TableCell>
              <TableCell align="center">Entered on</TableCell>
              <TableCell align="center">Won</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {dataDisplay.map((data: any) => (
              <TableRow>
                <TableCell component="th" scope="row">
                  {data.number}
                </TableCell>
                <TableCell align="right">{data.created_at}</TableCell>
                <TableCell align="right" style={{ color:'green', fontWeight: 'bolder' }}>{data.Won}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Card>
  )
}

export default UserDashboard