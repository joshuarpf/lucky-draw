import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import React from 'react'

// Components
import AdminDashboard from "./admin/AdminDashboard"
import UserDashboard from "./user/UserDashboard"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  }),
);

/**
 * Main function of the commponent
 * 
 * @param props contains tye type of dashboard to be loaded
 */
function Dashboard(props: any) {

  const dashboardToShow = (props.type === "admin_dashboard") ? <AdminDashboard /> : <UserDashboard />

  const classes = useStyles()

  return (
      <div>
        <CssBaseline />
        <Container fixed>
        { dashboardToShow }
        </Container>
      </div>
  )
}

export default Dashboard


