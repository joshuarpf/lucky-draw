export const isValidEntry = (input: string) => {
  if ((input != null) && (input !== '') && !isNaN(Number(input))) {
    // tslint:disable-next-line:ban
    const value = parseInt(input, 10)

    if( value > 0) {
      return true
    }

    return false
  }

  return false
}