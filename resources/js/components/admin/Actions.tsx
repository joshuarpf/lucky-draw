import axios from 'axios'
import React from 'react'

export const Draw = (priceType: string, winningNumber: string) => {
  // TODO: change this to implement dotenv

  return axios.post(
    process.env.MIX_APP_URL + '/admin/draw', {
      price_type: priceType,
      winning_number: ( winningNumber ) ? winningNumber: '0',
    }
  ).then((result) => {
    if(result && result.status === 200 && result.data) {
      return { status: result.status, data: result.data }
    }

    return null
  }).catch((error: any) => {
    if(error.response.status === 422) {
      return { status: error.response.status, data: error.response.data.errors }
    }
  })
}