import { ListItemSecondaryAction } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormGroup from '@material-ui/core/FormGroup'
import MenuItem from "@material-ui/core/MenuItem"
import Select from "@material-ui/core/Select"
import Switch from '@material-ui/core/Switch'
import TextField from "@material-ui/core/TextField"
import Typography from '@material-ui/core/Typography'
import React, { useState } from 'react'
import { isValidEntry } from "../Common"
import { Draw } from "./Actions"
import { useStyles } from "./Styles"

// tslint:disable-next-line:no-empty
function AdminDashboard() {

  const classes = useStyles()
  const [isRandomized, changeRandomizedValue] = useState(false)
  const [winningNumber, setWinningNumber] = useState("")
  const [winningNumberError, setWinningNumberError] = useState(false)
  const [priceType, setPriceType] = useState("")
  const [priceTypeError, setPriceTypeError] = useState("")
  const [displayProperty, setDisplayProperty] = useState({})
  const [resultingPriceType, setResultingPriceType] = useState('')
  const [resultingNumber, setResultingNumber] = useState('')

  const callDraw = async () => {
    const drawResult = await Draw(priceType, winningNumber)

    if( drawResult ) {
      if( drawResult.status === 200 ) {
        setResultingPriceType(drawResult.data.price_type)
        setResultingNumber(drawResult.data.number)
      } else if ( drawResult.status === 422 ) {

        if( drawResult.data.price_type && drawResult.data.price_type[0] ) {
          setPriceTypeError(drawResult.data.price_type[0])
        }
      }
    }
  }

  const changeRandomized = (value: boolean): void =>  {
    changeRandomizedValue(value)

    if(isRandomized) {
      setDisplayProperty({})
    } else {
      setDisplayProperty({ display: 'none' })
      setWinningNumber("")
    }
  }

  const changePriceType = (value: any): void => {
    switch(value) {
      case 'first':{
        setPriceType('first')
        break;
      }
      case 'second': {
        setPriceType('second')
        break;
      }
      case 'third': {
        setPriceType('third')
        break;
      }
      default:
        break;
    }
  }

  const updateWinningNumber = (value: string): void => {
    setWinningNumber(value)
    if( isValidEntry(value) ) {
      setWinningNumberError(false)
    } else {
      setWinningNumberError(true)
    }
  }

  return(
    <Card className={ classes.root }>
      <CardContent>
        <Typography gutterBottom variant="h4" component="h2">
          Lucky Draw 
        </Typography>
        <Typography gutterBottom variant="h6" component="h2">
          <p style={{ fontWeight:'bolder', color: 'red' }}>{ (resultingPriceType && resultingNumber) ? resultingPriceType + ' ' + resultingNumber : ''}</p>
        </Typography>
        <hr />
      </CardContent>
      <CardContent>
        <Typography component="h2" variant="h6">
          Prize Types*:
        </Typography>
        <Select
          labelId="demo-simple-select-outlined-label"
          onChange={ (event) => { changePriceType(event.target.value) } }
          value={ [priceType ]}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value="first">First Price</MenuItem>
          <MenuItem value="second">Second Price</MenuItem>
          <MenuItem value="third">Third Price</MenuItem>
        </Select>
        <span style={{ color: 'red', marginLeft: '10px' }}>
          { priceTypeError }
        </span>
      </CardContent>
      <CardContent>
        <FormGroup>
          <FormControlLabel
            control={<Switch checked={ isRandomized } onChange={ (event) => { changeRandomized( event.target.checked ) }}/>}
            label={ (isRandomized)? 'switch to turn random off': 'swtich to turn random on' }
          />
        </FormGroup>
      </CardContent>
      <CardContent>
        <div style={ displayProperty }>
          <TextField
            helperText={ (winningNumberError) ? 'Invalid entry':'' }
            error={ winningNumberError }
            id="outlined-basic"
            label="Winning number"
            variant="outlined"
            value={ winningNumber }
            onChange={ (event) => updateWinningNumber(event.target.value) }
          />
        </div>
      </CardContent>
      <CardActions>
        <Button disabled={ winningNumberError } color="primary" variant="contained" onClick={ () => { callDraw() } }>Draw</Button>
      </CardActions>
    </Card>

  )
}

export default AdminDashboard