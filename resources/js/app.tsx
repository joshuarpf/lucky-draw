import React from 'react'
import ReactDOM from 'react-dom'

import Dashboard from './components/Dashboard'

if (document.getElementById('admin_dashboard')) {
  ReactDOM.render(<Dashboard type="admin_dashboard"/>, document.getElementById('admin_dashboard'))
}

if (document.getElementById('user_dashboard')) {
  ReactDOM.render(<Dashboard type="user_dashboard"/>, document.getElementById('user_dashboard'))
}