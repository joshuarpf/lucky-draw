@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col col-lg-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                @if($is_admin)
                  <div id="admin_dashboard" style="margin: 30px;"></div>
                @else
                <div id="user_dashboard" style="margin: 30px;"></div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
