<?php

namespace App\Models;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

use App\Models\Draw;

class Number extends Model
{
  protected $fillable = ['number', 'user_id'];

  protected $hidden = [
    'user_id'
  ];

  protected $appends = ['Won'];

  protected $casts = [ 'number' => 'integer' ];

  /**
   * Returns a human radable format of the date
   *
   * @return void
   */
  public function getCreatedAtAttribute() {
    return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('Y-m-d');
  }

  /**
   * Returns a string to indicate what price a number has won
   *
   * @return void
   */
  public function getWonAttribute() {
    $hasWon = Draw::where('number', $this->attributes['number'])->first();

    if($hasWon)  {
      return $hasWon->price_type;
    }

    return "";
  }

  /**
   * Defines the relationship
   *
   * @return void
   */
  public function user() {
    return $this->belongsTo('App\Models\User');
  }
}
