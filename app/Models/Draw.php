<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Draw extends Model
{
  protected $fillable = ['price_type','number'];
}
