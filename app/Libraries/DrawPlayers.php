<?php

use Auth;

// Models
use App\Models\Number;
use App\Models\User;

class DrawPlayers {
  
  /**
   * Adds a number to the user's record of numbers
   *
   * @param [type] $number
   * @return void
   */
  public static function AddNumber($input) {
    try {
      $newNumber = new Number;
      $newNumber->number = $input;
      $newNumber->user_id = Auth::user()->id;

      $newNumber->save();  

      return $newNumber;
    } catch (Exception $exception) {
      echo "Exception caught in AddNumber: ".$exception;
    }
  }

  /**
   * Returns a collection of the user's numbers
   *
   * @return void
   */
  public static function getNumbers() {
    try {
      $user = User::with('numbers')->where('id', Auth::user()->id)->first();

      if($user && $user->numbers) {
        $numbers = $user->numbers;
        return $numbers;
      } else {
        return null;
      }
    } catch(Exception $exception) {
      echo "Caught exception in getNumbers: ".$exception;
    }
  }
}