<?php

// Models
use App\Models\Draw;
use App\Models\User;

class DrawAdmin {

  /**
   * Saves / Creates a new entry for a draw or winning
   *
   * @param [type] $priceType
   * @param [type] $winningNumber
   * @return void
   */
  public static function GenerateDraw($priceType, $winningNumber = 0) {
    try {
      $newDraw              = new Draw;
      $newDraw->price_type  = $priceType;
      $newDraw->number      = ($winningNumber > 0)? $winningNumber: self::getRandomNumber();
      $newDraw->save();

      return $newDraw;
    } catch(Exception $exception) {
      echo "Exception caught in GenerateDraw: ".$exception->getMessage();
    }
  }

  /**
   * Fetches a random number from the user with the most number counts
   *
   * @return void
   */
  private static function getRandomNumber() {
    $users                = self::getUserCollectionWithHighestNumberCounts();
    $numbersToRandomize   = self::getNumbersToRandomize($users); 
    $randomIndex          = rand(1, (count($numbersToRandomize) - 1));
    $randomNumber         =  $numbersToRandomize[$randomIndex];
    return $randomNumber;
  }

  /**
   * Returns a collection of numbers to randomize
   *
   * @param [type] $users
   * @return void
   */
  private static function getNumbersToRandomize($users) {
    try {
      $numbersToRandomize = [];

      foreach($users as $user) {
        foreach($user->numbers as $userNumber) {
          array_push($numbersToRandomize, $userNumber->number);
        }
      }

      return array_unique($numbersToRandomize);
    } catch(Exception $exception) {
      echo "Exception caught in getNumbersToRandomize: ".$exception->getMessage(); 
    }
  }

  /**
   * Returns an array of users with the highest number counts.
   *
   * @return void
   */
  private static function getUserCollectionWithHighestNumberCounts() {
    try {
      $users = User::where('id', '!=', 11)->withCount('numbers')->orderBy('numbers_count', 'desc')->get();

      $highestCount = $users[0]->numbers_count;

      $usersToReturn = [];
      array_push($usersToReturn, $users[0]); 

      for($iterator = 1; $iterator < count($users); $iterator++) {
        if($users[$iterator]->numbers_count < $highestCount) {
          break;
        }
      }

      return $usersToReturn;
    } catch(Exception $exception) {
      echo "Exception caught in getUserCollectionWithHighestNumberCounts: ".$exception->getMessage();
    }
  }
}