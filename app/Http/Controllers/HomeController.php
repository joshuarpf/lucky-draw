<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user() && Auth::user()->id) {
          $userId   = Auth::user()->id; 
          $name     = Auth::user()->name; 
          $isAdmin  = Auth::user()->is_admin;

          return view('home', [ 'user_id' => $userId, 'user_name' => $name, 'is_admin' => $isAdmin ]);
        } else {
          return redirect('/');
        }
        
    }
}
