<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Userrequest;

// Libraries
use DrawPlayers;

class UserController extends Controller
{
    /**
     * Executes the process of adding a number entered by the user
     *
     * @param Request $request
     * @return void
     */
    public function add(Userrequest $request) {

      $request->validated();

      $newNumber = $request->get('number');

      $result = DrawPlayers::AddNumber($newNumber);

      if ($result) {
        return json_encode($result);
      }

      return null;
    }

    /**
     * Returns all the numbers of a user
     *
     * @return void
     */
    public function numbers() {
      return json_encode(DrawPlayers::getNumbers());
    }
}
