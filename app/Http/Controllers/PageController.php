<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Helpers 

use App\Helpers\CommonHelper;

class PageController extends Controller
{
    public function index() {

      $data = CommonHelper::getLatestWinningNumbers();

      return view('welcome', [
        'first_place' => ($data->first_place && $data->first_place->number) ? $data->first_place->number: '',
        'second_place' => ($data->second_place && $data->second_place->number) ? $data->second_place->number: '' ,
        'third_place' => ($data->third_place && $data->third_place->number)? $data->third_place->number: ''
      ]);
    }
}