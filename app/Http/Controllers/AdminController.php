<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Drawrequest;

// Libraries
use DrawAdmin;

class AdminController extends Controller
{
  public function draw(Drawrequest $request) {

    $request->validated();

    $priceType      = $request->get('price_type');
    $winningNumber  = $request->get('winning_number');

    $result = DrawAdmin::GenerateDraw($priceType, intval($winningNumber));

    if($result) {
      return json_encode($result);
    }

    return null;
  }
}