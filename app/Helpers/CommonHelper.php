<?php

namespace App\Helpers;

// Models
use App\Models\Draw;
use Exception;

class CommonHelper {
  /**
   * Returns an object that has properties to represent the latest winning numbers
   *
   * @return void
   */
  public static function getLatestWinningNumbers() {
    $winningNumbers = new \stdClass;

    $winningNumbers->first_place = self::getWinningNumber('first');
    $winningNumbers->second_place = self::getWinningNumber('second');
    $winningNumbers->third_place = self::getWinningNumber('third');

    return $winningNumbers;
  }

  /**
   * Executes the process of fetching the data from the database
   *
   * @param [type] $type
   * @return void
   */
  private static function getWinningNumber($type) {
    try {
      $winninNumber = Draw::where('price_type', $type)->orderBy('created_at', 'desc')->first();

      if($winninNumber) {
        return $winninNumber;
      }

      return null;

    } catch (Exception $exception) {
      echo "Exception caught in getWinningNumber: ".$exception;
    }
  }
}