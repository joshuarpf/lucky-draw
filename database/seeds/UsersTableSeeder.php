<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;

// Models
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Creates the admin account
      DB::table('users')->insert([
        'name' => 'Admin',
        'email' => 'admin@luckydraw.com',
        'email_verified_at' => Now(),
        'is_admin' => 1,
        'password' => Hash::make('password'),
      ]);

      // Creates dummy accounts
      for($iterator = 0; $iterator <= env('NUMBER_OF_DUMMY_USERS', 10); $iterator++) {
        $fakeUser   = Factory(User::class)->make(); 
        
        $user                     = new User;
        $user->name               = $fakeUser->name;
        $user->email              = $fakeUser->email;  
        $user->email_verified_at  = Now(); 
        $user->password           = Hash::make('password');

        $user->save(); 

        $userNumber = mt_rand(1, 10); 
        for($innerIterator = 1; $innerIterator  <= $userNumber; $innerIterator++) {
          $randomNumber = mt_rand(10000, 99999);
          $user->numbers()->create(['number' => $randomNumber, 'user_id' => $user->id]);
        }
      }
    }
}